﻿namespace WatchDog.ConfigManages
{
    public enum ConfigChangedTypes
    {
        Add = 0,
        Update = 1,
        Del = 2,
    }
}
