﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory;

namespace WatchDog.ConfigManages
{
    public class ConfigManage
    {
        private ConcurrentDictionary<int, WatchItem> _cache = new ConcurrentDictionary<int, WatchItem>();

        public delegate void DataChangedHandler(object sender, ConfigChangedEventArgs e);
        public event DataChangedHandler DataChanged;

        public List<WatchItem> GetFileInfo()
        {
            if (_cache.IsEmpty)
            {
                using (var db = new SqliteDbContext())
                {
                    var list = db.QueryWatchItems.ToList();
                    foreach (var fileInfo in list)
                    {
                        _cache.AddOrUpdate(fileInfo.Id, fileInfo, (key, value) => value);
                    }
                    return list;
                }
            }
            return _cache.Values.ToList();
        }

        public void Add(WatchItem file)
        {
            using (var db = new SqliteDbContext())
            {
                db.WatchItems.Add(file);
                db.SaveChanges();

                _cache.Clear();
                DataChanged(this, new ConfigChangedEventArgs(ConfigChangedTypes.Add, new List<WatchItem>
                {
                    file
                })
                { });

            }
        }

        public void Del(int id)
        {
            using (var db = new SqliteDbContext())
            {
                var model = db.QueryWatchItems.FirstOrDefault(c => c.Id == id);
                if (model != null)
                {
                    db.WatchItems.Remove(model);
                    db.SaveChanges();

                    _cache.Clear();

                    DataChanged(this, new ConfigChangedEventArgs(ConfigChangedTypes.Del, new List<WatchItem>
                {
                    model
                })
                    { });
                }
            }
        }

        public void Update(int id, bool auto)
        {
            using (var db = new SqliteDbContext())
            {
                var model = db.QueryWatchItems.FirstOrDefault(c => c.Id == id);
                if (model != null)
                {
                    model.AutoStart = auto;
                    db.WatchItems.Update(model);
                    db.SaveChanges();
                    _cache.Clear();

                    DataChanged(this, new ConfigChangedEventArgs(ConfigChangedTypes.Update, new List<WatchItem>
                {
                    model
                })
                    { });
                }
            }
        }
        public void Update(WatchItem info)
        {
            using (var db = new SqliteDbContext())
            {
                var model = db.QueryWatchItems.FirstOrDefault(c => c.Id == info.Id);
                if (model != null)
                {
                    model.AutoStart = info.AutoStart;
                    model.DisplayName = info.DisplayName;
                    model.DelayStart = info.DelayStart;
                    model.FileName = info.FileName;
                    model.FilePath = info.FilePath;
                    model.Remark = info.Remark;
                    model.ScheduledRestart = info.ScheduledRestart;

                    db.WatchItems.Update(model);
                    db.SaveChanges();
                    _cache.Clear();

                    DataChanged(this, new ConfigChangedEventArgs(ConfigChangedTypes.Update, new List<WatchItem>
                {
                    model
                })
                    { });
                }
            }
        }

        public void ClearCache()
        {
            _cache.Clear();
        }
    }
}
