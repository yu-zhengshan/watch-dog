﻿using System;

namespace WatchDog.ConfigManages
{
    public class WatchItem
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string DisplayName { get; set; }
        public string FilePath { get; set; }
        /// <summary>
        /// 自动启动
        /// </summary>
        public bool AutoStart { get; set; }
        /// <summary>
        /// 启动时延时时长（s），有效值范围：0-定时重启范围内(如果有)
        /// </summary>
        public int DelayStart { get; set; }

        /// <summary>
        /// 定时重启（分），默认：0（不启用）
        /// </summary>
        public int ScheduledRestart { get; set; }

        public string Remark { get; set; }
    }
}
