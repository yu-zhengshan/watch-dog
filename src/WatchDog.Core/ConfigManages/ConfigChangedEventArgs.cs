﻿using System;
using System.Collections.Generic;

namespace WatchDog.ConfigManages
{
    public class ConfigChangedEventArgs : EventArgs
    {
        /// <summary>
        /// 变更内容
        /// </summary>
        public List<WatchItem> Items { get; private set; }
        /// <summary>
        /// 0-add 1-update 2-del
        /// </summary>
        public ConfigChangedTypes UpdateType { get; private set; }

        public ConfigChangedEventArgs(ConfigChangedTypes updateType, List<WatchItem> fileInfos)
        {
            Items = fileInfos;
            UpdateType = updateType;
        }
    }
}
