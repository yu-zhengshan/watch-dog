﻿using System;
using System.IO;
using System.Windows.Forms;
using WatchDog.ConfigManages;

namespace WatchDog.WinForm
{
    public partial class SettingForm : Form
    {
        private WatchItem _editItem;// = 0;
        private ConfigManage _configManage;// = new FileService();
 
     
        public SettingForm(ConfigManage service, WatchItem editItem = null)
        {
            InitializeComponent();

            this.numericUpDown1.Maximum = 60;
            this.numericUpDown1.Minimum = 0;

            this.numericUpDown2.Minimum = 0;
            this.numericUpDown2.Maximum = 8760; //24*365 一年
            this._configManage = service;

            if (editItem != null)
            {
                this._editItem = editItem;
                this.richTextBox1.Text = editItem.Remark;
                this.textBox1.Text = editItem.FilePath;
                this.textBox2.Text = editItem.DisplayName;
                this.checkBox1.Checked = editItem.AutoStart;
                this.numericUpDown1.Value = editItem.DelayStart;
                this.numericUpDown2.Value = editItem.ScheduledRestart / 60;
                if (editItem.AutoStart ==false)
                {
                    this.numericUpDown1.Enabled = false;
                }
            }

        }

        private void save_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.textBox1.Text))
            {
                MessageBox.Show("请选择要监控的程序路径", "提示", MessageBoxButtons.OK);
                return;
            }

            var autoStart = this.checkBox1.Checked;
            var a = this.numericUpDown1.Value;
            var b = this.numericUpDown2.Value;
            // 这种情况永远不会发生，numericUpDown1 最大值已经限制为 1分钟，numericUpDown2 的最小值则为1小时
            //if (autoStart && b > 0 && a > (b*60*60))
            //{
            //    MessageBox.Show("延时启动时长超过定时启动时长", "提示", MessageBoxButtons.OK,MessageBoxIcon.Warning);
            //    return;
            //}

            //if (_service != null)
            {
                var item = new WatchItem()
                {
                    Remark = this.richTextBox1.Text,
                    FilePath = this.textBox1.Text,
                    DisplayName = string.IsNullOrWhiteSpace(this.textBox2.Text) ? Path.GetFileName(this.textBox1.Text) : this.textBox2.Text,
                    FileName = Path.GetFileName(this.textBox1.Text),
                    AutoStart = autoStart,
                    DelayStart = (autoStart && a > 0) ? (int)Math.Ceiling(a) : 0,
                    ScheduledRestart = (int)Math.Ceiling(b * 60)
                };

                if (_editItem != null)
                {
                    item.Id = _editItem.Id; 
                    _configManage.Update(item);
                }
                else
                {
                    _configManage.Add(item);
                }
            }
            _editItem = null;
            _configManage = null;
            this.Close();
        }

        private void select_Click(object sender, EventArgs e)
        {
            var s = new OpenFileDialog();
            s.Filter = "(*.exe)|*.exe";
            s.Multiselect = false;
            if (s.ShowDialog() == DialogResult.OK)
            {
                this.checkBox1.Checked = true;
                this.textBox1.Text = s.FileName;// strNames[0];
                this.textBox2.Text = Path.GetFileName(this.textBox1.Text);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked)
            {
                this.numericUpDown1.Enabled = true;
            }
            else
            {
                this.numericUpDown1.Enabled = false;
            }
        }
    }
}
