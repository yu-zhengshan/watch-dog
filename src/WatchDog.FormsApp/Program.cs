﻿using Serilog;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WatchDog.WinForm
{
    internal static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            string strAssemblyFilePath = Assembly.GetExecutingAssembly().Location;
            string strAssemblyDirPath = Path.GetDirectoryName(strAssemblyFilePath);

            Serilog.loggingLevelSwitch.MinimumLevel = LogEventLevel.Debug;

            Log.Logger = new LoggerConfiguration()
           .MinimumLevel.ControlledBy(Serilog.loggingLevelSwitch)
           .WriteTo.File(strAssemblyDirPath + "/Logs/control-.log",
           rollingInterval: RollingInterval.Day,
           retainedFileTimeLimit: TimeSpan.FromDays(7),
           outputTemplate: "{Timestamp:HH:mm:ss.fff} [{Level:u3}] {Message:lj}{NewLine}{Exception}",
           fileSizeLimitBytes: 5242880)
           .CreateLogger();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            Application.ApplicationExit += (s, e) =>
            {
                Log.Logger.Debug(e.ToString(),"控制程序退出");
            };
        }
    }
}
